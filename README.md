# Convert PDF to Images

## Build Environment

```powershell
python -m venv .venv
.\.venv\Scripts\Activate.ps1
python -m pip install -r requirements.txt
```

## Run Script

```powershell
.\.venv\Scripts\Activate.ps1
python .\pdf2image.py
```

## Create Standalone Executable using PyInstaller

```powershell
python -m pip install pyinstaller==5.6
pyinstaller --onefile pdf2image.py --clean
```

Once pyinstaller finishedm, you should find an executable *.exe file in dest/
