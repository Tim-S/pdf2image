import argparse, os
import fitz # pip install pymupdf
from pathlib import Path

import tkinter as tk
from tkinter import filedialog

parser = argparse.ArgumentParser(description='Save PDF Pages as images.',
                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--image-format', dest='image_format', type=str, default="jpg",
                    help='Image Format')
parser.add_argument('--dest', dest='destination', type=str, default=None,
                    help='Destination-Path for the saved Images (one for each PDF-Page)')
parser.add_argument('pdf_file_paths_in', metavar='PDF_FILE', type=str, nargs='*',
                    help='PDF file')


args = parser.parse_args()
pdf_file_paths_in = args.pdf_file_paths_in
image_format = args.image_format
destination = args.destination

# To get better resolution
zoom_x = 2.0  # horizontal zoom
zoom_y = 2.0  # vertical zoom
mat = fitz.Matrix(zoom_x, zoom_y)  # zoom factor 2 in each dimension

# root = tk.Tk()

if(not pdf_file_paths_in):
    pdf_file_paths_in = filedialog.askopenfilename(initialdir = os.getcwd(), multiple=True, 
                                                filetypes = (("PDF files", "*.pdf"),("All files", "*.*") ))

print(pdf_file_paths_in if pdf_file_paths_in else "No PDF Files selected")

if(pdf_file_paths_in):
    if(not destination):
        destination = filedialog.askdirectory(initialdir = Path(pdf_file_paths_in[0]).resolve().absolute().parent)
    print(f"saving data to {destination}" )

    if(destination):
        for file_path_in in pdf_file_paths_in:
            _path = Path(file_path_in).resolve().absolute()
            _stem = _path.stem # filename without extension

            doc = fitz.open(_path)  # open document
            for page in doc:  # iterate through the pages
                pix = page.get_pixmap(matrix=mat)  # render page to an image
                _path_out = os.path.join(destination, f"{_stem}-page-{page.number}.{image_format}") 
                print(f"saving {_path_out}" )
                pix.save(_path_out)  # store image as a PNG